from django.db import models
from django.utils.timezone import now


class Employee(models.Model):
    created = models.DateTimeField(default=now)
    name = models.CharField(max_length=100, verbose_name="Name")
    email = models.EmailField(max_length=100, verbose_name="E-mail")
    department = models.CharField(max_length=50, verbose_name="Department")

    class Meta:
        ordering = ('name',)