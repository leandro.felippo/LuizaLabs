from django.apps import AppConfig


class LuizalabsEmployeeManagerConfig(AppConfig):
    name = 'Luizalabs_Employee_Manager'
